# TipCalculator Android Application <br/>

##### This is a simple project I created using Android Jetpack Compose.<br />
I created it after finishing first section on Udemy course "Android Jetpack Compose: The Comprehensive Bootcamp [2022]".<br/>
In this section I learned about structure of Jet and setting up container functions. I learned how to create simple layouts and <br />
I familiarized myself with with design parameters. Among the other things I learned about observables and troubleshooting common issues <br />
that can occur while compiling the code.<br />
<br />
At the header of the program there is text field that displays amount of money which is result from dividing the bill (with tip) and the number of persons.<br />
Program contains the single input field for the user to enter the amount of the bill.<br />
After entering the number in the input field user gets the button option to choose on what number of persons will the bill be divided on. <br />
User gets the slider option to choose what percentage of the bill will be put on the tip. Above the slider <br />
there is information about the tip, percentage of the bill and the amount of the tip. <br />
<br />
As the program uses the observables, every change on tip slider and number of persons is directly recalculated <br />
and updated on the header text field "Total per person".  

